\section{Local detection \label{local}}

\paragraph*{}
Once the global detection resulted in a set of entities' pairs that may interact together, exact information about the interaction at the local scale must be computed. It can be an intersection test, or more precisely the exact intersection topology if they intersect, or a distance computation for example. In this section, precise algorithms are presented to perform these different computations on different types of entities. 

\paragraph*{}
Webpage \hyperref{https://www.realtimerendering.com/intersections.html}{}{}{\underline{www.realtimerendering.com/intersections.html}} provides a large variety of algorithms to perform static and dynamic intersection of numerous 1D/2D/3D shapes and volumes. For few specific cases widely used in all sort of applications, like triangle/triangle or line/triangle static intersection, it may be useful to browse the scientific literature since the topic of finding faster algorithms is still active. Since it's not the purpose of this document, two more general algorithms are presented here for the general case of convex shapes intersection and distance computing, one is derived from the Separating Axis Theorem, the other is called the GJK distance algorithm.

\newpage
\subsection{Minimal distance \label{minimal distance}}

In this section, 4 distance algorithms between a point and different entities are presented. These functions are used in the \hyperref[GJK]{GJK algorithm}. Depending on the context, they need to return only a distance, only a direction, or both of them, hence the \textbf{and/or} statement when the function returns. In the context of distance comparisons, the squared distances should be used to avoid useless computation of square roots. $d$ is the minimal distance function.

\subsubsection{Point to point} 

\paragraph*{}
The distance between two points $P, A \in \mathbb{R}^3$ is defined as

\begin{equation*}
    d(P,A) = \sqrt{(x_A - x_P)^2 + (y_A - y_P)^2 + (z_A - z_P)^2}
\end{equation*}

\begin{algorithm}[H]
    \caption{Distance \textbf{and/or} direction between two points $P$ and $A$}
    \begin{algorithmic}[1]
    \Procedure{PointToPoint}{$P, A$}
        \State \Return $\sqrt{(x_A - x_P)^2 + (y_A - y_P)^2 + (z_A - z_P)^2}$ \textbf{and/or} $\vec{AP}$
    \EndProcedure    
    \end{algorithmic}
\end{algorithm}

\subsubsection{Point to line segment \label{PTS}}

\paragraph*{}
To compute the distance between a line segment $\left[AB\right]$ and a point $P$ for $A, B, P \in \mathbb{R}^3$, one must know the position of $P$ relative to the segment $\left[AB\right]$. The segment $\left[AB\right]$ defines 3 different zones shown in Figure \ref{PointSegment}. For example, if $P$ lies in Zone 2, then $B$ is the closest point to $P$ in $\left[AB\right]$.

\begin{figure}[H]
    \begin{center}
        \includegraphics[scale = 0.6]{Figures/Distances/PointSegment.pdf}
        \caption{Different zones for the position of $P$ to compute $d(P, \left[AB\right])$ \label{PointSegment}}
    \end{center}
\end{figure}

\paragraph*{}
The next equations discriminate the case of each zone and how to compute the distance.

\begin{equation*}
    d(\left[AB\right], P) = \left\{
    \begin{array}{cccl}
        &d(A,P) & \textrm{if} \quad \vec{AB} \cdot \vec{AP} < 0 & \textrm{(Zone 1)} \\
        &d(B,P) & \textrm{if} \quad \vec{AB} \cdot \vec{BP} > 0 & \textrm{(Zone 2)} \\
        &d((AB), P) = \frac{|\det(\vec{AP}, \vec{AB})|}{d(A,B)} & \textrm{otherwise} & \textrm{(Zone 3)}
    \end{array}
    \right.
\end{equation*}

\begin{algorithm}[H]
    \caption{Distance \textbf{and/or} direction between a point $P$ and a line segment $\left[AB\right]$}
    \begin{algorithmic}[1]
    \Procedure{PointToSegment}{$P, A, B$}
        \State $\vec{AB}, \vec{AP} := B - A, P - A$ \Comment given as input if available

        \item[]

        \If{$\vec{AB} \cdot \vec{AP} \leq 0$} \Comment $P$ is in zone 1
            \State \Return PointToPoint($A,P$)
        \EndIf
        \If{$\vec{AB} \cdot \vec{BP} \geq 0$} \Comment $P$ is in zone 2
            \State \Return PointToPoint($B,P$)
        \EndIf
        \State \Return $|\det(\vec{AP}, \vec{AB})|/||\vec{AB}||_2$ \textbf{and/or} $||\vec{AB}||_2\vec{AP} - (\vec{AP} \cdot \vec{AB})\vec{AB}$ \Comment $P$ is in zone 3
               
    \EndProcedure    
    \end{algorithmic}
\end{algorithm}

\subsubsection{Point to triangle}

\paragraph*{}
In the same manner as the point to line segment distance, one must know the position of a point $P$ relative to a triangle $ABC$ to compute the distance between $ABC$ and $P$, with $A, B, C, P \in \mathbb{R}^3$. The triangle $ABC$ defines 7 zones shown in Figure \ref{PointTriangle}. For example, if $P$ lies in Zone 2, then the orthogonal projection of P on $\left[AB\right]$ is the closest point to $P$ in $ABC$. For the sake of simplicity, the figure presents the 2D case, or a 3D case with a top view orthogonal to the triangle plane, the different lines separating different zones are always hyperplanes.

\begin{figure}[H]
    \begin{center}
        \includegraphics[scale = 0.6]{Figures/Distances/PointTriangle.pdf}
        \caption{Different zones for the position of $P$ to compute $d(P, ABC)$ \label{PointTriangle}}
    \end{center}
\end{figure}

\paragraph*{}
Different algorithms have been proposed in the literature \cite{jones_3d_nodate}, the 3D algorithm presented in this article projects $P$ on the triangle plane, compute its barycentric coordinates, and use them to identify in which zone $P$ belongs. This method requires a bit too many operations to be the fastest. The 2D algorithm of this article uses a Jacobian transformation matrix to transform the triangle to a simpler configuration. The computation of this matrix is presented as a preprocessing step, so it's not really a suitable solution in our context.

\paragraph*{}
However, these two algorithms are close to the one presented here, a 3D approach is used without using barycentric coordinates, and the final solution is highly similar to the 2D algorithm in \cite{jones_3d_nodate}. On figure \ref{PointTriangle}, one can see that the computation to know in which zone $P$ is lying, requires several point to plane positions to be computed, therefore a fast strategy must be found.

\begin{algorithm}[H]
    \caption{Distance \textbf{and/or} direction between a point $P$ and a triangle $ABC$}
    \begin{algorithmic}[1]
    \Procedure{PointToTriangle}{$P, A, B, C$}
        \State $\vec{AB}, \vec{BC}, \vec{CA}, \vec{AP} := B - A, C - B, A - C, P - A$ \Comment given as input if available
        \State $\vec{n} := (\vec{AB} \times \vec{BC})$  \Comment normal to the triangle

        \item[]

        \If{$(\vec{AB} \times \vec{n}) \cdot \vec{AP} \geq 0$} \Comment $P$ is on the side of $\vec{AB}$, either in zone 1, 2 or 3
            \State \Return PointToSegment($P, A, B$)
        \EndIf 
        \If{$(\vec{BC} \times \vec{n}) \cdot \vec{AP} \geq 0$} \Comment $P$ is in zone 3, 4 or 5
            \State \Return PointToSegment($P, B, C$)
        \EndIf 
        \If{$(\vec{CA} \times \vec{n}) \cdot \vec{AP} \geq 0$} \Comment $\vec{CA}$ could be computed just here, (zone 5, 6 or 1)
            \State \Return PointToSegment($P, A, C$)
        \EndIf 
        \State \Return $|\vec{AP} \cdot \vec{n}|/||\vec{n}||_2$ \textbf{and/or} $(\vec{AP} \cdot \vec{n})\vec{n}$\Comment distance/direction from plane $(ABC)$
               
    \EndProcedure    
    \end{algorithmic}
\end{algorithm}

The strategy presented here starts by finding on which side of each edge $P$ is lying, the rest is straightforward. Unlike the algorithms presented in the article, here $P$ is never projected on the plane $(ABC)$. If this projection lies on the plane (last case of the algorithm), the distance is directly computed. In every other cases, one can directly call the function PointToSegment defined previously in section \ref{PTS}, which will either call a PointToPoint distance function or project $P$ on an edge of the triangle, depending on the configuration (see figure \ref{PointSegment}). 

\subsubsection{Point to tetrahedron}

\paragraph*{}
Once the PointToTriangle algorithm presented above is understood, the adaptation for a PointToTetrahedron algorithm is straightforward. As with the triangle and the line segment, a point $P$ can lie in 15 different zones around a tetrahedron $ABCD$. 4 zones are closer to a vertex, 6 zones are closer to an edge, 4 zones are closer to a face, and the last zone is the inside of the tetrahedron. The algorithm starts by checking if $P$ is on the same side of each face as the vertex of $ABCD$ which doesn't belong to this face, and then either calls the PointToTriangle function or the distance is $0$ if $P$ is inside $ABCD$.

\begin{algorithm}[H]
    \caption{Distance \textbf{and/or} direction between a point $P$ and a tetrahedron $ABCD$}
    \begin{algorithmic}[1]
    \Procedure{PointToTetrahedron}{$P, A, B, C, D$}
        \State $\vec{AB}, \vec{AC}, \vec{AD}, \vec{AP} := B - A, C - A, D- A, P - A$ \Comment given as input if available

        \item[]

        \State $\vec{n}_{ABC} := (\vec{AB} \times \vec{AC})$  \Comment normal to the triangle $ABC$
        \If{$(\vec{n}_{ABC} \cdot \vec{AP}) * (\vec{n}_{ABC} \cdot \vec{AD}) < 0$} \Comment $P$ is not on the same side as $D$
            \State \Return PointToTriangle($P, A, B, C$)
        \EndIf

        \item[]
        
        \State $\vec{n}_{ABD} := (\vec{AB} \times \vec{AD})$  \Comment normal to the triangle $ABD$
        \If{$(\vec{n}_{ABD} \cdot \vec{AP}) * (\vec{n}_{ABD} \cdot \vec{AC}) < 0$} \Comment $P$ is not on the same side as $C$
            \State \Return PointToTriangle($P, A, B, D$)
        \EndIf

        \item[]

        \State $\vec{n}_{ACD} := (\vec{AC} \times \vec{AD})$  \Comment normal to the triangle $ACD$
        \If{$(\vec{n}_{ACD} \cdot \vec{AP}) * (\vec{n}_{ACD} \cdot \vec{AB}) < 0$} \Comment $P$ is not on the same side as $B$
            \State \Return PointToTriangle($P, A, C, D$)
        \EndIf

        \item[]

        \State $\vec{BC}, \vec{BD}, \vec{BP} := C - B, D - B, P - B$ \Comment given as input if available
        \State $\vec{n}_{BCD} := (\vec{BC} \times \vec{BD})$  \Comment normal to the triangle $ACD$
        \If{$(\vec{n}_{BCD} \cdot \vec{BP}) * (\vec{n}_{BCD} \cdot \vec{AB}) > 0$} \Comment $P$ is not on the same side as $A$
            \State \Return PointToTriangle($P, B, C, D$)
        \EndIf

        \item[]
            
        \State \Return $0$ \textbf{and/or} $\vec{0}$\Comment $P$ is inside $ABCD$
    \EndProcedure    
    \end{algorithmic}
\end{algorithm}

\subsubsection{Gilbert-Johnson-Keerthi distance algorithm (GJK algorithm) \label{GJK}}

\paragraph*{}
The GJK distance algorithm computes the minimal distance between two convex domains. It has been largely discussed since its publication \cite{gilbert_fast_1988} to adapt and optimize it for different applications related to collision detection or distance computations. More recent papers \cite{lindemann_gilbert-johnson-keerthi_nodate,vlack_fast_nodate} present clear explanations of the algorithm and some of its optimizations.

\paragraph*{}
It takes as inputs two convex domains described for example as a list of points for polyhedrons, and depends on two functions described below. Just a quick definition to begin : a simplex is any of the simplest (with regard to the number of points) non-degenerated entity of each dimension, that is either a point, a segment, a triangle, or a tetrahedron. To understand the NearestSimplex function, one says that a simplex of dimension $n > 0$ is composed of several sub-simplices of lower dimensions, for example a tetrahedron is composed of 14 sub-simplices of lower dimension : 4 points, 6 segments, and 4 triangles.

\begin{itemize}
    \item \textbf{Support(Shape, $\vec{d}$)} returns the point on Shape which has the highest dot product with $\vec{d}$, that is the furthest point along the direction $\vec{d}$. Its implementation is straightforward, some analytical shapes' Support functions are described in \cite{bergen_fast_1999}, and for any polyhedron a simple loop over the vertices is sufficient. It could also be optimized using a hill climbing strategy as suggested in \cite{vlack_fast_nodate}.
    \item \textbf{NearestSimplex(s)} takes a simplex s as input and returns the simplex on s closest to the origin, the direction and the distance from the closest point on s to the origin. It can return s itself in the first steps if no sub-simplex is closer to the origin than s (while the dimension of s is still lower than the dimension of the Minkowski difference), or at the last step if s contains the origin. Its implementation uses the PointToPoint, PointToSegment, PointToTriangle and PointToTetrahedron distance algorithms presented in section \ref{minimal distance}.
\end{itemize}

\paragraph*{}
Once these definitions are understood, the GJK distance algorithm's pseudocode presented below is quite straightforward, though its understanding can require more visual explanations.

\begin{algorithm}[H]
    \caption{Gilbert-Johnson-Keerthi distance algorithm \label{GJK algo}}
    \begin{algorithmic}[1]
    \Procedure{GJK}{Shape1, Shape2}
        \State $\vec{d}$ := (1, 0, 0) \Comment takes any direction to start with
        \State $\vec{v}$ := Support(Shape1, $\vec{d}$) - Support(Shape2, $-\vec{d}$)
        \State $s := \vec{v}$ \Comment simplex $s$ is the point of same coordinates as $\vec{v}$
        \State $\delta := ||\vec{v}||_2$ \Comment temporary distance of Minkowski difference to the origin
        \State $\vec{d}$ = $-\vec{v}$
        \While {$\delta \neq 0$}
            \State $\vec{v}_{old} := \vec{v}$
            \State $\vec{v}$ = Support(Shape1, $\vec{d}$) - Support(Shape2, $-\vec{d}$)
            \If {$||\vec{v} \cdot \vec{d}||_2 \geq ||\vec{v}_{old} \cdot \vec{d}||_2$} 
                \State \Return $\delta$
            \EndIf
            \State $s = s \cup \vec{v}$ \Comment point  $\vec{v}$ is added to $s$, which increases its dimension by 1
            \State $s$, $\vec{d}$, $\delta$ = NearestSimplex(s)
        \EndWhile
        \State \Return 0
    \EndProcedure    
    \end{algorithmic}
\end{algorithm}

\paragraph*{}
The core idea behind this algorithm is that the distance between two convex shapes is equal to the distance between their Minkowski difference and the origin, see figure \ref{Minkowski}. The Minkowski difference $S_1 - S_2$ of two convex shapes $S_1$ and $S_2$ is defined as the set of points which coordinates are the difference of the coordinates of a point from $S_1$ and the coordinates of a point from $S_2$.

\begin{equation*}
    S_1 - S_2 = \{z \in \mathbb{R}^3 \quad , \quad \exists (x, y) \in S_1 \times S_2 \quad , \quad z = x - y \}
\end{equation*}

The Minkowski difference of two convex shapes is convex. One can construct it by taking every pair of vertices of $S_1 \times S_2$, subtracting their coordinates to create a new set of vertices. The Minkowski difference is then the convex hull of this new set of vertices and its interior, as shown in figure \ref{Minkowski}. On this figure, the 9 points from G to O are defined as \mbox{G = A - D,} \mbox{H = A - E,} \mbox{... ,} \mbox{J = B - D,} \mbox{... ,} \mbox{O = C - F}.

\paragraph*{} The GJK algorithm doesn't compute the entire Minkowski difference since it would be a waste of time to compute every new vertex, and even worse to compute the convex hull. Instead, it selects two extremum values on $S_1$ and $S_2$ along one direction by calling the function Support, and construct a new point by subtraction. This new point is guaranteed to be on the convex hull of the Minkowski difference.

\begin{figure}[H]
    \begin{center}
        \begin{minipage}{0.49\textwidth}
            \begin{center}
                \includegraphics[scale=0.3]{Figures/GJK/Minkowski1.pdf}
                \caption*{Disjoint triangles : Minkowski \\ difference doesn't contain the origin}
            \end{center}
        \end{minipage}
        \begin{minipage}{0.49\textwidth}
            \begin{center}
                \includegraphics[scale=0.3]{Figures/GJK/Minkowski2.pdf}
                \caption*{Touching triangles : Minkowski \\ difference contains the origin}
            \end{center}            
        \end{minipage}
        \begin{center}
        \caption{Minkowski difference $S_1 - S_2$ with two triangles\label{Minkowski}}
        \end{center}
    \end{center}
\end{figure}

\paragraph*{}
The consecutive steps of the algorithm are presented in Figure \ref{GJK figure}, only red points of the Minkowski difference are computed, gray ones are only there for illustration. 

\paragraph*{} The aim of the main loop is to find the direction of the origin relative to the current simplex being analyzed in the loop. In the case where the origin is not contained in this simplex, NearestSimplex returns the closest sub-simplex to the origin. A new point is then added to the new simplex in the direction of the origin, this way one gets simplices closer and closer to the origin at each iteration.

\begin{figure}[H]
    \begin{center}
        \begin{minipage}{0.24\textwidth}
            \begin{center}
                \includegraphics[scale=0.28]{Figures/GJK/init.pdf}
                \caption*{End of initialization}
            \end{center}
        \end{minipage}
        \begin{minipage}{0.24\textwidth}
            \begin{center}
                \includegraphics[scale=0.28]{Figures/GJK/step1.pdf}
                \caption*{End of first iteration}
            \end{center}            
        \end{minipage}
        \begin{minipage}{0.24\textwidth}
            \begin{center}
                \includegraphics[scale=0.28]{Figures/GJK/step2.1.pdf}
                \caption*{Second iteration before NearestSimplex}
            \end{center}
        \end{minipage}
        \begin{minipage}{0.24\textwidth}
            \begin{center}
                \includegraphics[scale=0.28]{Figures/GJK/step2.2.pdf}
                \caption*{End of second iteration}
            \end{center}
        \end{minipage}
        \begin{center}
        \caption{Computing steps of the GJK distance algorithm. \\
        Gray points are not still (or never) computed \label{GJK figure}}
        \end{center}
    \end{center}
\end{figure}

\begin{itemize}
    \item \textbf{Initialization :} The algorithm starts with an initial direction $d = (1, 0, 0)$ (not shown on the figures) and finds the extremum value of $S_1$ and $S_2$ in this direction, the two points C and F. It subtracts their coordinates to build the point \mbox{O = C - F} of the Minkowski difference which defines the first simplex s = O. $d$ becomes the direction from O to the origin at the end of initialization.
    \item \textbf{First iteration :} The two extremum points along the direction $d$ are A and D, their subtraction gives the point G (the furthest point of the Minkowski difference in the direction $d$). The simplex s is redefined as the segment OG. Then NearestSimplex returns OG and $d$ is redefined as the direction between the closest point of OG to the origin and the origin.
    \item \textbf{Second iteration :} The two extremum points along the direction $d$ computed in the step 1 are C and D, their subtraction gives the point M. M is added to s which becomes the triangle $OGM$ before the call of NearestSimplex. This call then returns the new simplex $s = [GM]$ which is the simplex on the triangle $OGM$ closest to the origin.
    \item \textbf{Third iteration :} $\vec{v}$ won't change its value because the calls to the Support function will result in the coordinates of $M$ once again. Therefore, the algorithm stops in the if statement because $||\vec{v} \cdot \vec{d}||_2 = ||\vec{v}_{old} \cdot \vec{d}||_2$. The minimal distance $\delta$ between $S_1$ and $S_2$ is the distance from $[GM]$ to the origin. 
\end{itemize}

\subsection{Intersection (space only) \label{local intersection}}

In this section, two general algorithms are presented. They both test the intersection of any pair of convex polygons or polyhedrons. First one is the SAT algorithm, it is really easy to understand and implement, but suffers from its time complexity in 3D. It can at least serve as a validation test for the second algorithm, which is a specialization of the GJK distance algorithm presented in section \ref{GJK}. GJK algorithm also has the benefit of providing more information about the intersection if needed.

\subsubsection{Separating Axis Theorem (SAT) \label{SAT}}

\paragraph*{}
The Separating Axis Theorem states that if two convex domains do not intersect, then there exists a hyperplane separating the two domains strictly. An orthogonal axis to this hyperplane is called a separating axis. When the two domains are projected on a separating axis, their projections result in two disjoint segments on the axis.

\paragraph*{}
At this point, it seems difficult to use the SAT to find whether two entities are intersecting or not, since one must check every space axes among all possible directions to find a separating axis. The key idea that allows this approach to be very efficient, is that only few axes have to be tested. Once this set of axes is known, one must project the entities on every axis, and test the intersection of the resulting pair of segments. If at least one pair of projected segments do not overlap, then separating axis is found, thus the two entities do not intersect. Otherwise, both projected segments of every pair overlap, then the two entities intersect.

\begin{figure}[H]
    \begin{center}
        \includegraphics[scale = 0.8]{Figures/SAT.pdf}
        \caption{Separating axes with the SAT algorithm for a 2D planar intersection test \\
        The shapes' projections on the blue axis won't overlap, so the test returns false \label{SAT figure}}
    \end{center}
\end{figure}

\paragraph*{}
The directions of the axes to test in 2D are the entities' edges' normal vectors (as shown in Figure \ref{SAT figure}). In 3D, these directions are the entities' faces' normal vectors, but also every cross product between an edge of the first entity and an edge of the second one. This gives a very general procedure that can be quickly applied to any convex entity type, see algorithms \ref{SAT2D} and \ref{SAT3D}.

\begin{algorithm}[H]
    \caption{2D intersection of convex shapes with Separating Axis Theorem \label{SAT2D}}
    \begin{algorithmic}[1]
    \Procedure{IntersectConvex2D}{Shape1, Shape2}
        \ForAll {edge1 \textbf{in} Shape1} \Comment axis orthogonal to an edge of Shape1
            \State min1, min2 := $+\infty$; max1, max2 := $-\infty$  \Comment borders along axis
            \ForAll {vertex \textbf{in} Shape1} \Comment project Shape1 on the axis
                \State p := vertex $\cdot$ \textbf{normal}(edge1) \Comment project vertex on the axis
                \State min1 = \textbf{min}(min1, p); max1 = \textbf{max}(max1, p) \Comment update borders
            \EndFor
            \ForAll {vertex \textbf{in} Shape2} \Comment project Shape2 on the axis
                \State p := vertex $\cdot$ \textbf{normal}(edge1) \Comment project vertex on the axis
                \State min2 = \textbf{min}(min2, p); max2 = \textbf{max}(max2, p) \Comment update borders
            \EndFor

            \item[]

            \If{max1 $<$ min2 \textbf{or} min1 $>$ max2} \Comment projected segments don't overlap
                \State \Return \textbf{false}
            \EndIf       
        \EndFor

        \item[]

        \ForAll {edge2 \textbf{in} Shape2} \Comment axis orthogonal an edge of Shape2
            \State min1, min2 := $+\infty$; max1, max2 := $-\infty$  \Comment borders along axis
            \ForAll {vertex \textbf{in} Shape1} \Comment project Shape1 on the axis
                \State p := vertex $\cdot$ \textbf{normal}(edge2) \Comment project vertex on the axis
                \State min1 = \textbf{min}(min1, p); max1 = \textbf{max}(max1, p) \Comment update borders
            \EndFor
            \ForAll {vertex \textbf{in} Shape2} \Comment project Shape2 on the axis
                \State p := vertex $\cdot$ \textbf{normal}(edge2) \Comment project vertex on the axis
                \State min2 = \textbf{min}(min2, p); max2 = \textbf{max}(max2, p) \Comment update borders
            \EndFor

            \item[]

            \If{max1 $<$ min2 \textbf{or} min1 $>$ max2} \Comment projected segments don't overlap
                \State \Return \textbf{false}
            \EndIf       
        \EndFor

        \item[]

        \State \Return \textbf{true}
    \EndProcedure    
    \end{algorithmic}
\end{algorithm}

\begin{algorithm}[H]
    \caption{3D intersection of convex shapes with Separating Axis Theorem \label{SAT3D}}
    \begin{algorithmic}[1]
    \Procedure{IntersectConvex3D}{Shape1, Shape2}
        \ForAll {face1 \textbf{in} Shape1} \Comment axis orthogonal to a face of Shape1
            \State min1, min2 := $+\infty$; max1, max2 := $-\infty$  \Comment borders along axis
            \ForAll {vertex \textbf{in} Shape1} \Comment project Shape1 on the axis
                \State p := vertex $\cdot$ \textbf{normal}(face1) \Comment project vertex on the axis
                \State min1 = \textbf{min}(min1, p); max1 = \textbf{max}(max1, p) \Comment update borders
            \EndFor
            \ForAll {vertex \textbf{in} Shape2} \Comment project Shape2 on the axis
                \State p := vertex $\cdot$ \textbf{normal}(face1) \Comment project vertex on the axis
                \State min2 = \textbf{min}(min2, p); max2 = \textbf{max}(max2, p) \Comment update borders
            \EndFor

            \item[]

            \If{max1 $<$ min2 \textbf{or} min1 $>$ max2} \Comment projected segments don't overlap
                \State \Return \textbf{false}
            \EndIf       
        \EndFor

        \item[]

        \ForAll {face2 \textbf{in} Shape2} \Comment axis orthogonal an face of Shape2
            \State min1, min2 := $+\infty$; max1, max2 := $-\infty$  \Comment borders along axis
            \ForAll {vertex \textbf{in} Shape1} \Comment project Shape1 on the axis
                \State p := vertex $\cdot$ \textbf{normal}(face2) \Comment project vertex on the axis
                \State min1 = \textbf{min}(min1, p); max1 = \textbf{max}(max1, p) \Comment update borders
            \EndFor
            \ForAll {vertex \textbf{in} Shape2} \Comment project Shape2 on the axis
                \State p := vertex $\cdot$ \textbf{normal}(face2) \Comment project vertex on the axis
                \State min2 = \textbf{min}(min2, p); max2 = \textbf{max}(max2, p) \Comment update borders
            \EndFor

            \item[]

            \If{max1 $<$ min2 \textbf{or} min1 $>$ max2} \Comment projected segments don't overlap
                \State \Return \textbf{false}
            \EndIf       
        \EndFor

        \item[]
        
        \ForAll {edge1 \textbf{in} Shape1}
            \ForAll {edge2 \textbf{in} Shape2} 
                \State d := edge1 $\times$ edge2 \Comment axis orthogonal to an edge1 and edge2
                \State min1, min2 := $+\infty$; max1, max2 := $-\infty$  \Comment borders along axis
                \ForAll {vertex \textbf{in} Shape1} \Comment project Shape1 on the axis
                    \State p := vertex $\cdot$ d \Comment project vertex on the axis
                    \State min1 = \textbf{min}(min1, p); max1 = \textbf{max}(max1, p) \Comment update borders
                \EndFor
                \ForAll {vertex \textbf{in} Shape2} \Comment project Shape2 on the axis
                    \State p := vertex $\cdot$ d \Comment project vertex on the axis
                    \State min2 = \textbf{min}(min2, p); max2 = \textbf{max}(max2, p) \Comment update borders
                \EndFor

                \item[]

                \If{max1 $<$ min2 \textbf{or} min1 $>$ max2} \Comment projected segments don't overlap
                    \State \Return \textbf{false}
                \EndIf
            \EndFor     
        \EndFor

        \State \Return \textbf{true}
    \EndProcedure    
    \end{algorithmic}
\end{algorithm}

\subsubsection{GJK intersection algorithm}

The algorithm presented in section \ref{GJK} can be specialized to test only if two convex domains intersect. All distance algorithms between a point and another entity presented in section \ref{minimal distance} don't need to compute the final distance, only the direction is needed by NearestSimplex. Another major optimization is an earlier stop condition. Indeed, if at any time $\vec{v} \cdot \vec{d} < 0$, it means that $\vec{d}$ is a separating axis direction vector \cite{vlack_fast_nodate} (separating axes are defined in section \ref{SAT}). So there's no intersection in this case.

\begin{algorithm}[H]
    \caption{Gilbert-Johnson-Keerthi intersection algorithm}
    \begin{algorithmic}[1]
    \Procedure{GJKIntersect}{Shape1, Shape2}
        \State $\vec{d}$ := (1, 0, 0) \Comment takes any direction to start with
        \State $\vec{v}$ := Support(Shape1, $\vec{d}$) - Support(Shape2, $-\vec{d}$)
        \State $s := \vec{v}$ \Comment simplex $s$ is the point of same coordinates as $\vec{v}$
        \State $\vec{d}$ = $-\vec{v}$
        \While {$\vec{d} \neq \vec{0}$}
            \State $\vec{v}$ = Support(Shape1, $\vec{d}$) - Support(Shape2, $-\vec{d}$)
            \If {$\vec{v} \cdot \vec{d} < 0$} 
                \State \Return \textbf{false}
            \EndIf
            \State $s = s \cup \vec{v}$ \Comment point  $\vec{v}$ is added to $s$, which increases its dimension by 1
            \State $s$, $\vec{d}$ = NearestSimplex(s)
        \EndWhile
        \State \Return \textbf{true}
    \EndProcedure    
    \end{algorithmic}
\end{algorithm}

\begin{figure}[H]
    \begin{center}
        \begin{minipage}{0.32\textwidth}
            \begin{center}
                \includegraphics[scale=0.32]{Figures/GJK/init.pdf}
                \caption*{End of initialization}
            \end{center}
        \end{minipage}
        \begin{minipage}{0.32\textwidth}
            \begin{center}
                \includegraphics[scale=0.32]{Figures/GJK/step1.pdf}
                \caption*{End of first iteration}
            \end{center}            
        \end{minipage}
        \begin{minipage}{0.32\textwidth}
            \begin{center}
                \includegraphics[scale=0.32]{Figures/GJK/step2.0.pdf}
                \caption*{End of second iteration \\ (if statement)}
            \end{center}
        \end{minipage}
        \begin{center}
        \caption{Computing steps of the GJK intersection algorithm. \\
        Gray points are not still (or never) computed}
        \end{center}
    \end{center}
\end{figure}

\begin{itemize}
    \item \textbf{Initialization :} The algorithm starts with an initial direction $d = (1, 0, 0)$ (not shown on the figures) and finds the extremum value of $S_1$ and $S_2$ in this direction, the two points C and F. It subtracts their coordinates to build the point \mbox{O = C - F} of the Minkowski difference which defines the first simplex s = O. $d$ becomes the direction from O to the origin at the end of initialization.
    \item \textbf{First iteration :} The two extremum points along the direction $d$ are A and D, their subtraction gives the point G (the furthest point of the Minkowski difference in the direction $d$). The simplex s is redefined as the segment OG. Then NearestSimplex returns OG and $d$ is redefined as the direction between the closest point of OG to the origin and the origin.
    \item \textbf{Second iteration :} The two extremum points along the direction $d$ computed in the step 1 are C and D, their subtraction gives the point M. The function returns in the if statement because $\vec{v} \cdot \vec{d} < 0$.
\end{itemize}

\subsection{Collision (space-time intersection)}

\paragraph*{}
In order to follow the history of an entity between two time steps of a computational mechanics simulation, it might be useful to track their trajectories in time to detect an intersection that would have been missed with a static test at each time step. Note that these space-time tests are really identifying if there exists an instant $t$ between two time steps, where the entities are intersecting. This strategy is also known as extrusion or 4D intersection. As explained in \cite{vlack_fast_nodate} from an original description of \cite{cameron_collision_1990}, it's a stronger result than a sweeping volume intersection, which only test if the trajectories have crossed each other.

\paragraph*{}
A point to triangle space-time intersection test is presented here, more complex tests may be added in the future if useful. This test can be used to test the collision between a point and any polygon or polyhedron which surface can be triangulated. Tests between non deformable and non rotating polyhedrons are presented in \cite{vlack_fast_nodate}, but these hypothesis are too restrictive for computational mechanics.

\subsubsection{Point to triangle}

\paragraph*{}
This section presents an algorithm to test if a moving point $P$ has crossed a moving (and deformable) triangle $ABC$. The four points $P, A, B$ and $C$ are supposed to have a rectilinear uniform movement between two instants $t_0$ and $t_1$, so their speed is constant. This hypothesis is consistent with the evolution of the meshes' elements' positions between two time steps in computational mechanics. Without loss of generality, let's assume $t_0 = 0$ and $t_1 = 1$.

\paragraph*{}
Let $\vec{\Delta}_{AP} = \vec{A'P'} - \vec{AP}$, $\vec{\Delta}_{AB} = \vec{A'B'} - \vec{AB}$ and $\vec{\Delta}_{AC} = \vec{A'C'} - \vec{AC}$ be respectively the variation of $\vec{AP}$, $\vec{AB}$ and $\vec{AC}$ between $t_0$ and $t_1$.

\begin{figure}[H]
    \begin{center}
        \includegraphics[scale = 0.8]{Figures/Collision/initialCollision.pdf}
        \caption{Initial problem for point triangle collision detection}
    \end{center}
\end{figure}

\paragraph*{}
To figure out if $P$ is passed through the triangle $ABC$, one first needs to compute if and when $P$ passed through the plane $(ABC)$. This consists in finding the roots of the following function.

\begin{equation*}
    f(t) = \vec{AP}(t) \cdot \vec{n}(t) = (\vec{AP} + t\vec{\Delta}_{AP}) \cdot \biggl((\vec{AB} + t\vec{\Delta}_{AB}) \times (\vec{AC} + t\vec{\Delta}_{AC})\biggr)
\end{equation*}

\begin{equation*}
    \begin{array}{llc}
        f(t) &= &\vec{\Delta}_{AP} \cdot (\vec{\Delta}_{AB} \times \vec{\Delta}_{AC})t^3 \\
        &+ &\biggl(\vec{AP} \cdot (\vec{\Delta}_{AB} \times \vec{\Delta}_{AC}) + \vec{\Delta}_{AP} \cdot (\vec{\Delta}_{AB} \times \vec{AC} + \vec{AB} \times \vec{\Delta}_{AC})\biggr) t^2 \\
        &+ &\biggl(\vec{AP} \cdot (\vec{\Delta}_{AB} \times \vec{AC} + \vec{AB} \times \vec{\Delta}_{AC}) + \vec{\Delta}_{AP} \cdot (\vec{AB} \times \vec{AC}) \biggr) t \\
        &+ &\vec{AP} \cdot (\vec{AB} \times \vec{AC})
    \end{array}
\end{equation*}

\paragraph*{}
Now one must solve the cubic equation $f(t) = 0$, and check if its roots are between $0$ and $1$. If such roots exist, they are time of collision of $P$ and the plane $(ABC)$, between the two time steps. The last thing to do is to know if the corresponding collision point of the plane $(ABC)$ lie in the triangle $ABC$.

\paragraph*{}
Let $t_i$ be a root of $f$, $\vec{AP}_i = \vec{AP} + t_i\vec{\Delta}_{AP}$, $\vec{AB}_i = \vec{AB} + t_i\vec{\Delta}_{AB}$ and $\vec{AC}_i = \vec{AC} + t_i\vec{\Delta}_{AC}$. A fast way to compute the barycentric coordinates of $P_i$ from $\vec{AP}_i, \vec{AB}_i$ and $\vec{AC}_i$ is then

\begin{algorithm}[H]
    \caption{Compute the barycentric coordinates of a point in a triangle}
    \begin{algorithmic}[1]
    \Procedure{BarycentricCoordinates}{$\vec{AP}_i, \vec{AB}_i, \vec{AC}_i$}
        \State $d_{00} := \vec{AB}_i \cdot \vec{AB}_i$
        \State $d_{11} := \vec{AC}_i \cdot \vec{AC}_i$
        \State $d_{01} := \vec{AB}_i \cdot \vec{AC}_i$
        \State $d_{02} := \vec{AB}_i \cdot \vec{AP}_i$
        \State $d_{12} := \vec{AC}_i \cdot \vec{AP}_i$
        \item[]
        \State denom $:= 1/(d_{00}*d_{11} - d_{01}^2)$
        \item[]
        \State $b_1 := $ denom $*(d_{11}*d_{02} - d_{01}*d_{12})$
        \State $b_2 := $ denom $*(d_{00}*d_{12} - d_{01}*d_{02})$
        \State $b_3 := 1 - b_1 - b_2$
        \item[]
        \State \Return $b_1, b_2, b_3$
    \EndProcedure    
    \end{algorithmic}
\end{algorithm}

\paragraph*{}
If every barycentric coordinates are positive, then the collision point is inside the triangle and the collision test is true, otherwise it is false.